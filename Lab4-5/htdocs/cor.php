
<!DOCTYPE html>
<html lang="ru" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.min.css" >
  <link rel="stylesheet" href="css/main.css" >
<link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&family=Ranchers&display=swap" rel="stylesheet">
<link rel="shortcut icon" href="photo/logoblack.png">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>


<title>Pretty Sweets</title>
</head>
<style >
  .carouselExampleIndicators {
  height: 500px !important;
}
.carousel-inner img {
  height: 600px !important;
  margin: 0 auto;
}
/*.carousel-indicators li {
  border-color: #000;
}
.carousel-indicators .active {
  background-color: #000;
}
</style>
<body>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-2" src="photo/1.jpg" alt="Первый слайд">
    </div>
    <div class="carousel-item">
      <img class="d-block w-2" src="photo/1.jpg" alt="Второй слайд">
    </div>
    <div class="carousel-item">
      <img class="d-block w-2" src="photo/1.jpg" alt="Третий слайд">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>






</body>
</html>
