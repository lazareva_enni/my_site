<?php
session_start();
include "header1.php";

?>
<!DOCTYPE html>
<html lang="ru" >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.min.css" >
	<link rel="stylesheet" href="css/main.css" >
<link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&family=Ranchers&display=swap" rel="stylesheet">
<link rel="shortcut icon" href="photo/logoblack.png">


<title>Pretty Sweets</title>
	<style>
		.table{
			color: #808080;
		}
		.assritem{
		margin-left: 35%;
		font-size: 35px;
		}
body{
	background:  url(photo/fon.jpg) no-repeat ;


}


</style>
</head>

<body style="background-attachment:fixed" topmargin="0" >
	 <header>

<div class="container">
<div>
<figure><img src="photo/1.jpg" alt="" class="img"></figure>
			
	 			<div class="col-lg-2 ml-auto">
	 				<nav>
	 					<ul class="assr d-flex justife-content-center">
	 						<li class="assr__item">
	 							
	 								ПИРОЖНЫЕ
	 			
	 						</li>
	 					</ul>
	 				</nav>
	 			</div>
	 		</div>
	 		
<img src="photo/shok.jpg" alt="" class="img">
	
	 			<div class="col-lg-2 ml-auto">
	 				<nav>
	 					<ul class="assr d-flex justife-content-center">
	 						<li class="assr__item">
	 							
	 								ШОКОЛАД
	 			
	 						</li>
	 					</ul>
	 				</nav>
	 			</div>
	 		
<img src="photo/truf.jpg" alt="" class="img">
			
	 			<div class="col-lg-2 ml-auto">
	 				<nav>
	 					<ul class="assr d-flex justife-content-center">
	 						<li class="assr__item">
	 							РУЧНАЯ РАБОТА
	 						</li>
	 					</ul>
	 				</nav>
	 			</div>
	 		
</div>

<div class="container">
	 		<div class="row">
	 			
	 			<div class="diviz">
	 				<p align="center" align="justify">Сладкая жизнь войдет в привычку! Больше, чем кондитерская.<br>Аппетитные снаружи, аппетитные внутри.Сладости от лучших кондитеров!</p>	
	 					
	 				
	 			</div>
	 			
	 		</div>
	 	</div>

                          <li class="assritem">
	 							Заявка на заказ товара
	 						</li>
  
							 <h2>Добавить заказ</h2>
    <form action="insert_into.php" method="POST">
        <div>
            <label for="Name">Имя</label>
            <input type="text" id="Name" name="zakaz_name" required>
        </div>
        <div>
            <label for="Email">e_mail</label>
            <input type="text" id="Email" name="zakaz_email" required>
        </div>
        <div>
        <div style="display: inline-block; vertical-align: top;">
            <label>Заказ </label>
        </div>
        <div style="display: inline-block; vertical-align: top;">
            <input type="checkbox" id="zakaz_chok" name="zakaz_genre[]" value="Шоколад">
            <label for="zakaz_chok">Шоколад</label><br>
            <input type="checkbox" id="zakaz_tort" name="zakaz_genre[]" value="Торт">
            <label for="zakaz_tort">Торт</label><br>
            <input type="checkbox" id="zakaz_chiz" name="zakaz_genre[]" value="Чизкейк">
            <label for="zakaz_chiz">Чизкейк</label><br>
            <input type="checkbox" id="zakaz_pon" name="zakaz_genre[]" value="Пончики">
            <label for="zakaz_pon">Пончики</label><br>
            <input type="checkbox" id="zakaz_pir" name="zakaz_genre[]" value="Пирожное">
            <label for="zakaz_pir">Пирожное</label>
        </div>
        <div>
            <label for="Kol">Количество</label>
            <input type="number" step="1" min="0" id="Kol" name="zakaz_quantity">
		</div>
		<div>
            <label for="Date">Дата</label>
            <input type="text" id="Date" name="zakaz_date" required>
        </div>
        <input type="submit" value="Отправить в БД">
    </form>
	<h2>Редактировать данные</h2>
    <form action="update2.php" method="POST">
        <div>
            <label for="id">Выберите ID строки</label>
            <input type="number" id="id" name="zakaz_id" required>
        </div>
        <div>
            <label for="name">Имя</label>
            <input type="text" id="name" name="zakaz_name">
        </div>
        <div>
            <label for="email">e-mail</label>
            <input type="text" id="email" name="zakaz_email">
        </div>
        <div>
            <label for="product">Заказ</label>
            <input type="text" id="product" name="zakaz_product">
        </div>
        <div>
            <label for="quantity">Количество</label>
            <input type="number" min="0" max="100" id="quantity" name="zakaz_quantity">
        </div>
        <div>
            <label for="date">Дата</label>
            <input type="text" id="date" name="zakaz_date">
        </div>
        <input type="submit" value="Обновить запись">
    </form>			 			
<table class="table_col">
<colgroup>
      <col style="background:#C7DAF0;">
  </colgroup>



 
	<tr>
	    <th>№</th>
		<th>Имя</th>
		<th>e-mail</th>
		<th>Заказ</th>
		<th>Количество</th>
		<th>Дата</th>
		<th>Удалить</th>
    </tr>	
<?php
$link = @new mysqli('localhost', 'root', 'root', 'table');
  if (mysqli_connect_errno()) {
    echo "Подключение невозможно: ".mysqli_connect_error();
  }
  
  $sql = mysqli_query($link, 'SELECT*FROM zakaz');
  while ($res = mysqli_fetch_array($sql)) {
	
	echo "<form action='delete.php' method='POST'>";
	
	 echo "<tr><td>" . $res['id'] . "</td><td>" . $res['name'] . "</td><td>" . $res['email'] . "</td><td>" . $res['product'] . "</td><td>" . $res['quantity'] . "</td><td>" . $res['date'] . "</td><td><input type='checkbox' name='delete_row[]' value='" . $res["id"] . "'></td></tr>"; 
  }
  echo "<br><input type='submit' value='Удалить выделенные записи'></form>"; 
   mysql_close();
?>
</table>

	 </header>
	 </body>
	 </html>
