<?php
$db_server = "localhost";
$db_user = "root";
$db_password = "root";
$db_name = "table"; 
try {
    // Открываем соединение, указываем адрес сервера, имя бд, имя пользователя и пароль,
    // также сообщаем серверу в какой кодировке должны вводится данные в таблицу бд.
    $db = new PDO("mysql:host=$db_server;dbname=$db_name", $db_user, $db_password,array(PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES utf8"));
    // Устанавливаем атрибут сообщений об ошибках (выбрасывать исключения)
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // Переносим данные из полей формы в переменные.
    $zakaz_name=$_POST['zakaz_name'];
    $zakaz_email=$_POST['zakaz_email'];
    $zakaz_quantity=$_POST['zakaz_quantity'];
    $zakaz_date=$_POST['zakaz_date'];
 
    // Переносим данные (отмеченные жанры) из полей формы в массив
    $book_genres = array();
    
    if(!empty($_POST['zakaz_genre'])){
        foreach($_POST['zakaz_genre'] as $genre_selected){
            $zakaz_genres[] = $genre_selected;
        }
    }
    
    // Используем Prepared statements (заранее скомпилированное SQL-выражение) для защиты от SQL-инъекций.
    // Создаем ассоциативный массив для подстановки данных в запрос.
    $data = array(
        'name' => "$zakaz_name",
        'email' => "$zakaz_email",
        'quantity' => "$zakaz_quantity",
        'date' => "$zakaz_date",
    );
 
    // Запрос на создание записи в таблице
    // Если есть хоть один отмеченный жанр в форме, то составляем запрос, внося все отмеченные жанры,
    // иначе название жанра не вносим в таблицу.
    if(sizeof($zakaz_genres) > 0){
        $sql = "INSERT INTO zakaz(name, email, product, quantity,date)".
    " VALUES(:name, :email,'" . implode(',', $zakaz_genres) . "', :quantity, :date)";
    } else {
        $sql = "INSERT INTO zakaz(name, email, quantity, date)".
    " VALUES(:name, :email, :quantity, :date)";
    }
   
    
    // Подготовка запроса (замена псевдо переменных :title, :author и т.п. на реальные данные)
    $statement = $db->prepare($sql);
    // Выполняем запрос
    $statement->execute($data);
    
    echo "Запись успешно создана!";
}
 
catch(PDOException $e) {
    echo "Ошибка при создании записи в базе данных: " . $e->getMessage();
}
 
// Закрываем соединение
$db = null;
?>